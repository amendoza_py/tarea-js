// Declarar un vector de 6 elementos con diferentes tipos de datos
let vector = [10, 'Hola', true, 3.14, false, 'Mundo'];

// Imprimir el vector en la consola
console.log(vector);

// Imprimir el primer y último elemento del vector usando sus índices
console.log('Primer elemento:', vector[0]);
console.log('Último elemento:', vector[vector.length - 1]);

// Modificar el valor del tercer elemento
vector[2] = 'Adiós';

// Imprimir la longitud del vector
console.log('Longitud del vector:', vector.length);

// Agregar un elemento al final del vector usando "push"
vector.push(42);

// Eliminar el último elemento del vector y mostrarlo en la consola usando "pop"
const ultimoElemento = vector.pop();
console.log('Último elemento eliminado:', ultimoElemento);

// Agregar un elemento en la mitad del vector usando "splice"
vector.splice(3, 0, 'OpenAI');

// Eliminar el primer elemento del vector y mostrarlo en la consola usando "shift"
const primerElemento = vector.shift();
console.log('Primer elemento eliminado:', primerElemento);

// Agregar de nuevo el mismo elemento al inicio del vector usando "unshift"
vector.unshift(primerElemento);

// Imprimir el vector actualizado en la consola
console.log(vector);
