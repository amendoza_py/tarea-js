// Declarar un vector de 6 elementos con diferentes tipos de datos
let vector = [10, 'Hola', true, 3.14, false, 'Mundo'];

// Imprimir cada valor del vector utilizando "for"
console.log('Imprimir usando "for":');
for (let i = 0; i < vector.length; i++) {
  console.log(vector[i]);
}

// Imprimir cada valor del vector utilizando "forEach"
console.log('Imprimir usando "forEach":');
vector.forEach((valor) => {
  console.log(valor);
});

// Imprimir cada valor del vector utilizando "map"
console.log('Imprimir usando "map":');
vector.map((valor) => {
  console.log(valor);
});

// Imprimir cada valor del vector utilizando "while"
console.log('Imprimir usando "while":');
let index = 0;
while (index < vector.length) {
  console.log(vector[index]);
  index++;
}

// Imprimir cada valor del vector utilizando "for..of"
console.log('Imprimir usando "for..of":');
for (let valor of vector) {
  console.log(valor);
}
